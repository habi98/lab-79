CREATE SCHEMA `inventory` DEFAULT CHARACTER SET utf8 ;

USE `inventory` ;

CREATE TABLE `categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `places` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_Id` INT NOT NULL,
  `placesId` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(150) NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`category_Id` ASC),
  INDEX `places_id_fk_idx` (`placesId` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`category_Id`)
    REFERENCES `categories` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `places_id_fk`
    FOREIGN KEY (`placesId`)
    REFERENCES `places` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);
    
INSERT INTO `categories` (`id`, `title`)
VALUES
  (1, 'Furniture'),
  (2, 'Computer equipment'),
  (3, 'Appliances');
  
INSERT INTO `places` (`id`, `title`)
VALUES
     (1, 'Director s office'),
     (2, 'Office 204'),
     (3, 'Teacher s');
     
INSERT INTO `items` (`id`, `category_Id`, `placesId`, `title`)
VALUES
  (1, 2, 2, 'HP Probook 450 Laptop'),
  (2, 1, 2, 'Chair computer KK-345'),
  (3, 3, 3, 'Coffee machine Philips LatteGo Series 5000');